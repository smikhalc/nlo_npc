#/!bin/bash                                                                                                                                                                                                 
let "JS = 1"
let "seed = 0"

for entry in `ls $search_dir*lhe*`;
do
#    mv ${entry} user.smikhalc.21902946.EXT0._${seed}.PowhegOTF._1.lhe.events                                                                                                                               
echo ${entry}
pathena --trf "Generate_tf.py --ecmEnergy=13000. --maxEvents=100000 --runNumber=10000 --firstEvent=1 --randomSeed=${JS}${seed} --outputEVNTFile=%OUT.pool.root --jobConfig=mc.Py8_A14NNPDF23_jj.py --inputG\
eneratorFile=_1 --env NPC_EFFECT=Parton" --extFile=RivetJETS.so,MC15JobOptions,${entry} --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_Parton_${seed}_massPr_2.vt\
est
pathena --trf "Generate_tf.py --ecmEnergy=13000. --maxEvents=100000 --runNumber=10000 --firstEvent=1 --randomSeed=${JS}${seed} --outputEVNTFile=%OUT.pool.root --jobConfig=mc.Py8_A14NNPDF23_jj.py --inputG\
eneratorFile=_1 --env NPC_EFFECT=UEHad" --extFile=RivetJETS.so,MC15JobOptions,${entry} --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_UEHad_${seed}_massPr_2.vtes\
t
#    pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=Parton" --extFile\
=RivetJETS.so,${entry} --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_Parton_${seed}.vtest                                                                        
#    pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=NOShower" --extFi\
le=RivetJETS.so,$entry --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_NOShower_${seed}.vtest                                                                      
#    pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=UE" --extFile=Riv\
etJETS.so,$entry --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_UE_${seed}.vtest                                                                                  
#    pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=UE+Had" --extFile\
=RivetJETS.so,$entry --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_UEHad_${seed}.vtest                                                                           
#    pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=Hadr" --extFile=R\
ivetJETS.so,$entry --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_Hadr_${seed}.vtest                                                                              
    let "seed = seed + 1"
    #echo $seed                                                                                                                                                                                             
done
