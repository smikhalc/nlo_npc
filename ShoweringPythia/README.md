The NP correction is usually calculated using Monte-Carlo (MC) event generators with phenomenological models for hadronisation and UE as the bin-wise ratio of the differential jet cross sections obtained with and without NP effects. 
Based on the above u need to generate spectra of particles with NP effects on and spectra of partons (ME after showering) with MC generators.
 
In case of NLO generators it will be better to generate LHE fales with information about ME kinematics and then shower them.
Powheg LHE generation described here: https://gitlab.cern.ch/smikhalc/nlo_npc/-/tree/master/LHEGeneration
In this dir u can find scripts for showering LHE files with Pythia.

Directory 100000 contain JO for LHE showering of dijet (inclusive jet) production. 
Directory Matching contain scripts for Matching both level distributions and send jobs to HTC Condor for speed up this process.
LeadingJets.cc -- Rivet routine
rivet-buildplugin_local -- for local Rivet 2 routine compilation

JSample should be specified in previous LHE generation step. At this step, we are just adding shower and NP effects to the already sampled files. 

##
## Initialize environment
##

```
source setup.sh (*)
```

* current setup.sh does

asetup 19.2.4.12.1,MCProd,64

which contains next generator versions:

Pythia8 v.210

## Compile routine

```
rivet-build --with-root RivetJETS.so LeadingJets.cc
```

## Load LHE. For example from /nfs/dust/atlas/user/star/Mikhalcov/Powheg_pt2/user.smikhalc.Ph_LHE_100000_JS1_pt2.vtest_EXT0. Run example:

```
Generate_tf.py --ecmEnergy=13000. --maxEvents=100000 --runNumber=10000 --firstEvent=1 --randomSeed=12345 --outputEVNTFile=pool.root --jobConfig=mc.Py8_A14NNPDF23_jj.py --inputGeneratorFile=_1 --env NPC_EFFECT=Parton
```
* where:
--jobConfig=mc.Py8_A14NNPDF23_jj.py JO inside folder with name --runNumber=10000.

--inputGeneratorFile=_1 Generate_tf search LHE file with pattern `*`lhe`*`events. In case of files from from /nfs/dust/atlas/user/star/Mikhalcov/Powheg_pt2/user.smikhalc.Ph_LHE_100000_JS1_pt2.vtest_EXT0 value _1 will work.

--NPC_EFFECT = ["Parton", "Hadr", "UE", "UEHad", "NOShower"] NP effect. 

Shower with Parton and then UEHad for standart NP correction.

A lot of work have been already done. Check /nfs/dust/atlas/user/star/Mikhalcov/Powheg_pt2 .
In this folder u can find 9 samples dir`s with corresponding 400 LHE files (*._1.lhe.events) and showered TreeJets.root files with effects (inside folders UEHad) and without them (inside folders Parton). About LHE u can find more info in https://gitlab.cern.ch/smikhalc/nlo_npc/-/tree/master/LHEGeneration . 
Also there u can find scripts for PanDa sending jobs and Rucio downloading (runProd.sh and rucioDown.sh). All production in this directory is debugged and fully functional, you just need to change the number of output files. Check README to full process description.
## Main steps of production:

* Generate LHE (https://gitlab.cern.ch/smikhalc/nlo_npc/-/tree/master/LHEGeneration); 
* Download LHE (rucioDown.sh);
* Send jobs to Panda with Parton and UEHad NP effects (runProd.sh); 
* Download showered files (rucioDown.sh)
* Renamed showered root files (rename.sh)
* Matching (at this step filled matrix and other neccessary hists defined in Matching.C). Use Matching.submit for parallelizing this proccess with HTCCondor.
* Merge and normalize (merge.py)
