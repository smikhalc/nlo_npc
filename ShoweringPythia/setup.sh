#!/bin/bash

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export DQ2_LOCAL_SITE_ID=DESY-HH_SCRATCHDISK 
source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh

asetup 19.2.4.12.1,MCProd,64
#asetup AthGeneration,21.6.29, here

export RIVET_ANALYSIS_PATH=$PWD     

