import os
import shutil

### To get the list of files to be saved do:
### for i in `ls`; do echo "'"${i}"',";done
###


###
### Check carefully this list of file to be saved, others will be removed
###

doNotRemove=[
    '.git',  
    'JS2.user.smikhalc.22697024.EXT0._0.PowhegOTF._1.lhe.events',
    'JS3.user.smikhalc.22697024.EXT0._0.PowhegOTF._1.lhe.events',
    'JS4.user.smikhalc.21902981.EXT0._000001.PowhegOTF._1.lhe.events',
    'JS9.user.smikhalc.21903038.EXT0._000001.PowhegOTF._1.lhe.events',
    'findInfo.py',
    'GenerateJO.py',
    'GenerateJO_PY8_EIG.py',
    'GenerateJONew_PY8_EIG.py',
    'group.phys-gener.powheg_000311.361284.jj_A14NNPDF23_JZ4_13TeV.TXT.mc15_v1._00001.tar.gz',
    'user.smikhalc.22697024.EXT0._0.PowhegOTF._1.lhe.events',
    'Ph.10.TXT.lhe.events',
    'Generators',
    'MC15JobOptions',
    '100000',
    'InstallArea',
    'README',
    'rivetenv.sh',
    'Matching.submit',
    'Matching.sh',
    'Matching.C',
    'RivetJETS.so',
    'rivet-buildplugin_local',
    'rivet-build',
    'Result',
    'logjets.txt',
    'logjets2.txt',
    'logparticle.txt',
    'logparticle2.txt',
    'InclusiveJets.cc',
    'LeadingJets.cc',
    'clean.py',
    'runBatchArray.sh',
    'setup.sh',
    'setupCT10.sh',
    'submitBatch.sh',
    'yoda2root.py',
    'setFilterEfficiency.C',
    'share',
    'monitoring.sh',
    'myjob.submit',
    'myjobPowheg.submit',
    'HTCrunBatch.sh',
    'logs',
    'Results',
    'macroes',
    'BatchOutput'
    ]
allFilesInDir=os.listdir(".")





def notification():
    yes = set(['yes','y', 'ye'])
    no  = set(['no','n'])
    print "Are you sure you want to remove everything except: ", doNotRemove
    print "yes/no ?\n"
    choice = raw_input().lower()
    if choice in yes:
        print "Removing....\n"
        return True
    elif choice in no:
        print "Exiting. No action\n"
        exit(-1)
    else:
        notification()
        
notification()

for i in allFilesInDir:
    if i not in doNotRemove:
        try:
            os.remove(str(i))
        except EnvironmentError:
            shutil.rmtree(str(i))
        except Exception,e:
            print e, "No action"
            
print "Removed.\n"
