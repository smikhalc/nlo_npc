#### Shower

EFFECT=""
try:
    EFFECT=str(os.environ['NPC_EFFECT'])
except KeyError:
    print "WARNING: using default effect"
    EFFECT="Parton"
    print "ERROR: Wrong effect set in environment"
    
print 'EFFECT =', EFFECT

if EFFECT=='Parton':
    TYPE = 'NS'
    
#elif runArgs.runNumber==100001: #UE+Had                                                          
elif EFFECT=='UEHad':
    TYPE = 'AH'
    
#elif runArgs.runNumber==100002: #UE                                                               
elif EFFECT=='UE':
    TYPE = 'AS'
    
#elif runArgs.runNumber==100003: #Hadr                                                             
elif EFFECT=='Hadr':
    TYPE = 'NH'

elif EFFECT=='NOShower': ### special for Powheg+Pythia8                                            
    TYPE = 'OP'

else:
    print 'ERROR: Wrong EFFECT!'
    exit()


evgenConfig.description = "POWHEG+Pythia8 dijet production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "QCD", "jets", "2jet"]
evgenConfig.contact = ["amoroso@cern.ch"]
evgenConfig.generators = ['Pythia8']

if hasattr(testSeq, "TestHepMC"):
    testSeq.remove(TestHepMC())
    

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_Common.py")                      
include("MC15JobOptions/Pythia8_Powheg_Main31.py")

ANALYSES = ['LeadingJets']                                                                         

from Rivet_i.Rivet_iConf import Rivet_i                                                            

rivet = Rivet_i()                                                                                  
                        

rivet.AnalysisPath = os.getenv("PWD")                                                              
rivet.OutputLevel = INFO                                                                           
rivet.Analyses = ANALYSES                                                                          
genSeq += rivet    
