// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Tools/Logging.hh"
#include "Rivet/Projections/FastJets.hh"

#include "Rivet/Particle.fhh"

#include "TH1D.h"
#include "TFile.h"
#include "TTree.h"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/JetAlg.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Projections/LeadingParticlesFinalState.hh"

#include <fstream>
#include <vector>
#include <sstream>
#include <string> 

#include <fastjet/contrib/SoftDrop.hh>

namespace Rivet{

  // const int nAlg = 1;
  // const string AlgName[nAlg] = { "AKT04" };
  // const double R      [nAlg] = { 0.4 };

  const int nAlg = 12;
  const string AlgName[nAlg] = { "AKT04", "AKT04SDB00Z10", "AKT04SDB10Z10", "AKT04SDB20Z10",
				 "AKT06", "AKT06SDB00Z10", "AKT06SDB10Z10", "AKT06SDB20Z10",
				 "AKT10", "AKT10SDB00Z10", "AKT10SDB10Z10", "AKT10SDB20Z10"};
  const double R      [nAlg] = { 0.4,     0.4,             0.4,             0.4,
				 0.6,     0.6,             0.6,             0.6,
				 1.0,     1.0,             1.0,             1.0};

  const int nybins = 7;
  const string yBinsNames[nybins]   = { "0", "1",  "2",  "3",  "4",  "5",  "6" };
  const double yBinsEdges[nybins+1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 5.0 };
  const double yStarBins [nybins+1] = { 0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 5.0 };

  class JETS : public Analysis 
  {
    
  public:
    
    JETS() : Analysis("LeadingJets_WithSD") { /*setNeedsCrossSection(true);*/ }
    
  private:

    TFile *file;

    TTree *tree;
    
    TH1D *h_nFiles;
    TH1D *h_xs;
    TH1D *h_nEvents;
    TH1D *h_sumOfWeights;
    TH1D *h_genFiltEff;
    
    double *event_weight;
    int *jet_N;
    std::vector<double> *jet_E;                                                                                                                      
    std::vector<double> *jet_Pt;                                                                                                                      
    std::vector<double> *jet_M;                                                                                                                      
    std::vector<double> *jet_Rap;                                                                                                                     
    std::vector<double> *jet_Eta;                                                                    
    std::vector<double> *jet_Phi;

    long   evt;
    double sumw;
    
    int eventNumber;
    double eventWeight;
    double getFiltEff;

    /// Histograms
    vector<double> betas;
    
  public:
    
    float getXSfromLog()
    {
      cout << "getting cross-section from log" << endl;
      string cfilename="log.generate";
      std::ifstream fileInput;
      fileInput.open(cfilename.c_str());
      char* search = "MetaData: cross-section (nb)=";
      unsigned int curLine = 0;
      string line;
      while(getline(fileInput, line))  // I changed this, see below
	{
	  curLine++;
	  if (line.find(search, 0) != string::npos) 
	    {
	      stringstream ss(line); 
	      string buf; 
	      vector<string> tokens;
	      while (ss >> buf)
		tokens.push_back(buf);
	      cout << "found: " << search << "line: " << curLine << endl;
	      cout << "XS=" << tokens[tokens.size()-1] <<endl;
	      fileInput.close();
	      return atof(tokens[tokens.size()-1].c_str());
	    }
	}
      fileInput.close();
      cout<< "not found :(" <<endl;
      return 0;
    }
    
    float getGetGenFiltEffFromLog()
    {
      cout << "getting GenFiltEff from log" << endl;
      string cfilename="log.generate";
      std::ifstream fileInput;
      fileInput.open(cfilename.c_str());
      char* search = "Total efficiency: ";
      unsigned int curLine = 0;
      string line;
      while(getline(fileInput, line)) 
	{
	  curLine++;
	  if (line.find(search, 0) != string::npos) 
	    {
	      stringstream ss(line); 
	      string buf; 
	      vector<string> tokens;
	      while (ss >> buf)
		tokens.push_back(buf);
	      tokens[tokens.size()-5].pop_back();
	      cout << "found: " << search << "line: " << curLine << endl;
	      cout << "GenFiltEff=" << atof(tokens[tokens.size()-5])/100 << endl;
	      fileInput.close();
	      return atof(tokens[tokens.size()-5].c_str())/100;
	    }
	}
      fileInput.close();
      cout<< "not found :(" <<endl;
      return 1.0;
    }
        
    void init() 
    {
      std::cout << "**** I'm in init() Rivet function" << std::endl;
      
      file = new TFile("TreeJets.root","recreate");

      tree = new TTree("TreeJets","A Tree with jets");

      h_nFiles       = new TH1D("nFiles",       "Number of files",      1, 0, 2);
      h_xs           = new TH1D("xs",           "Total cross section",  1, 0, 2);
      h_nEvents      = new TH1D("nEvents",      "Number of events",     1, 0, 2);
      h_sumOfWeights = new TH1D("sumOfWeights", "Sum of event weights", 1, 0, 2);
      h_genFiltEff   = new TH1D("genFiltEff",   "Filter efficiency",    1, 0, 2);
      
      event_weight = new double[nAlg]; 
      jet_N   = new int[nAlg];
      jet_E   = new vector<double>[nAlg];
      jet_Pt  = new vector<double>[nAlg];
      jet_M   = new vector<double>[nAlg];                                                                                                         
      jet_Rap = new vector<double>[nAlg];                                                                                            
      jet_Eta = new vector<double>[nAlg];                                          
      jet_Phi = new vector<double>[nAlg];
      

      const FinalState fs(Cuts::abseta < 10);/*-5.0, 5.0, 0.);*/

      for(int iAlg=0; iAlg<nAlg; iAlg++) 
	{
	  //cout << "nAlg = " << iAlg << endl;
	  FastJets j( fs, FastJets::ANTIKT, R[iAlg] );
	  j.useInvisibles(true);
	  declare(j,AlgName[iAlg]);//addProjection(j,AlgName[iAlg]);
	}

      tree->Branch("eventNumber",&eventNumber);
      tree->Branch("eventWeight",&eventWeight);

      for(int iAlg=0; iAlg<nAlg; iAlg++) 
	{	  
	  tree->Branch( (AlgName[iAlg]+string("_event_weight")). c_str(), &event_weight  [iAlg] );
	  tree->Branch( (AlgName[iAlg]+string("_jet_N")).        c_str(), &jet_N         [iAlg] );
	  tree->Branch( (AlgName[iAlg]+string("_jet_E")).        c_str(), &jet_E         [iAlg] );
	  tree->Branch( (AlgName[iAlg]+string("_jet_M")).        c_str(), &jet_M         [iAlg] );
	  tree->Branch( (AlgName[iAlg]+string("_jet_Pt")).       c_str(), &jet_Pt        [iAlg] );
	  tree->Branch( (AlgName[iAlg]+string("_jet_Rap")).      c_str(), &jet_Rap       [iAlg] );
	  tree->Branch( (AlgName[iAlg]+string("_jet_Eta")).      c_str(), &jet_Eta       [iAlg] );
	  tree->Branch( (AlgName[iAlg]+string("_jet_Phi")).      c_str(), &jet_Phi       [iAlg] );
	}

      evt  = 0;
      sumw = 0;

      betas = { 0., 1., 2. };
      
      std::cout << "**** Finished with init()" << std::endl;
    }

    
    void analyze(const Event& event) 
    {
      //std::cout << "**** I'm in analyze(...) function: Event " << evt << std::endl;
      
      /// First, clear all variables for each new event.
      
      for(int iAlg=0; iAlg<nAlg; iAlg++) 
	{
	  eventNumber = 0;
	  eventWeight = 0.0;

	  event_weight [iAlg] = 0;
	  jet_N  [iAlg] = 0;

	  jet_E  [iAlg].clear();
	  jet_M  [iAlg].clear();
	  jet_Pt [iAlg].clear();
	  jet_Rap[iAlg].clear();
	  jet_Eta[iAlg].clear();
	  jet_Phi[iAlg].clear();
	}
      

      double cut_jet_pt = 10.0;
      double cut_jet_y  = 10.0;
      
      evt++;
      if ( evt%1000 == 0 ) 
	cout<<evt<<endl;

      eventNumber = evt;
      eventWeight = event.weight();
      //cout << event.weight() << endl;
      //cout << event.weights() << endl;

      sumw = sumw + eventWeight;

      Jets jets[nAlg];
      for( size_t iAlg=0; iAlg < nAlg; iAlg++ ) 
	{
	  //	  foreach ( const Jet& jet, apply/*Projection*/<FastJets>(event, AlgName[iAlg]).jetsByPt(Cuts::pT > cut_jet_pt*GeV && Cuts::abseta < 5.0/*cut_jet_pt*GeV*/))
	  const Jets _jets = applyProjection<FastJets>(event, AlgName[iAlg]).jetsByPt(Cuts::pT > cut_jet_pt*GeV && Cuts::absrap < cut_jet_y);
	  //cout << AlgName[iAlg] << " number of jets = " << _jets.size() << endl; 

          for ( size_t ijet=0; ijet<_jets.size(); ijet++)
            {
              //FourMomentum jmom;
              PseudoJet pjmom;
              if( (AlgName[iAlg]=="AKT04") || (AlgName[iAlg]=="AKT06") || (AlgName[iAlg]=="AKT10"))
                {
                  pjmom = _jets[ijet].momentum();
                  //cout<<"ug jet, pt="<<pjmom.perp()<<endl;
                  if ( fabs(pjmom.rapidity()) < cut_jet_y )
                    {
                      jets[iAlg].push_back(_jets[ijet]);
                    }
                }
              else if(    (AlgName[iAlg]=="AKT04SDB00Z10") || (AlgName[iAlg]=="AKT06SDB00Z10") || (AlgName[iAlg]=="AKT10SDB00Z10")
                       || (AlgName[iAlg]=="AKT04SDB10Z10") || (AlgName[iAlg]=="AKT06SDB10Z10") || (AlgName[iAlg]=="AKT10SDB10Z10")
                       || (AlgName[iAlg]=="AKT04SDB20Z10") || (AlgName[iAlg]=="AKT06SDB20Z10") || (AlgName[iAlg]=="AKT10SDB20Z10"))
                {
                  // According to G. Soyez, should use max R here to avoid pathological situations
		  fastjet::ClusterSequence cs_ca(_jets[ijet].constituents(),
						 fastjet::JetDefinition(fastjet::cambridge_algorithm,
                                                      fastjet::JetDefinition::max_allowable_R));
                  PseudoJets _jets_ca = sorted_by_pt(cs_ca.inclusive_jets(cut_jet_pt));
                  if(_jets_ca.size()==0) continue;

                  float beta=-1.;
                  if((AlgName[iAlg]=="AKT04SDB00Z10") || (AlgName[iAlg]=="AKT06SDB00Z10") || (AlgName[iAlg]=="AKT10SDB00Z10")) beta=0.;
                  if((AlgName[iAlg]=="AKT04SDB10Z10") || (AlgName[iAlg]=="AKT06SDB10Z10") || (AlgName[iAlg]=="AKT10SDB10Z10")) beta=1.;
                  if((AlgName[iAlg]=="AKT04SDB20Z10") || (AlgName[iAlg]=="AKT06SDB20Z10") || (AlgName[iAlg]=="AKT10SDB20Z10")) beta=2.;
                  fastjet::contrib::SoftDrop sd(beta, 0.1); //beta, zcut

                  PseudoJet _sdJet = sd(_jets_ca[0]);
                  pjmom = sd(_jets_ca[0]);

                  if ( fabs(pjmom.rapidity()) < cut_jet_y )
                    {
                      jets[iAlg].push_back(sd(_jets_ca[0]));
                    }
                }
              //cout << "E = " << jmom.E() << endl;
            }
	  
	  /*
	  for ( size_t ijet=0; ijet<_jets.size(); ijet++)
	    {
	      PseudoJet pjmom;
	      if(iAlg<=2)
		{
		  pjmom = _jets[ijet].momentum();
		  if ( fabs(pjmom.rapidity()) < cut_jet_y )
		    {
		      jets[iAlg].push_back(_jets[ijet]);
		    }
		}
	      if(iAlg>2 )
		{
		  // According to G. Soyez, should use max R here to avoid pathological situations
		  ClusterSequence cs_ca(_jets[ijet].constituents(),
					JetDefinition(fastjet::cambridge_algorithm,
						      fastjet::JetDefinition::max_allowable_R));
		  PseudoJets _jets_ca = sorted_by_pt(cs_ca.inclusive_jets(cut_jet_pt));
		  if(_jets_ca.size()==0) continue;

		  // Beta is hardcoded based on iAlg ... this is a bit nasty. -- MLB
		  fastjet::contrib::SoftDrop sd(betas[(iAlg+3)%3], 0.1); //beta, zcut
		  PseudoJet _sdJet = sd(_jets_ca[0]);
		  pjmom = sd(_jets_ca[0]);
		  if ( fabs(pjmom.rapidity()) < cut_jet_y )
		    jets[iAlg].push_back(sd(_jets_ca[0]));
		}
	      //cout << "E = " << jmom.E() << endl;
	    }
	  */
	}
	     
      for( size_t iAlg=0; iAlg<nAlg; iAlg++)
	{
	  jet_N[iAlg] = 0;
	  event_weight[iAlg] = 0;
	  jet_N[iAlg] = jets[iAlg].size(); 
	  event_weight [iAlg] = event.weight();;
	  if (jet_N[iAlg] < 1 ) 
	    continue;
	  
	  for ( size_t iJet=0 ; iJet < jet_N[iAlg]; iJet++ )
	    {
	      PseudoJet pjmom = jets[iAlg][iJet];
	      jet_E  [iAlg].push_back( pjmom.E()  / GeV );
	      jet_M  [iAlg].push_back( pjmom.m() / GeV );
	      jet_Pt [iAlg].push_back( pjmom.perp()  / GeV );
	      jet_Eta[iAlg].push_back( pjmom.eta() );
	      jet_Phi[iAlg].push_back( pjmom.phi() );
	      jet_Rap[iAlg].push_back( pjmom.rapidity() );	     	      
	    }
	}
      tree->Fill();      
    }
    
    void finalize() 
    {
      
      std::cout << "I'm in finalize() function" << std::endl;
      //std::cout << "Needs cross-section " << needsCrossSection() <<std::endl;
      std::cout << "SumOfWeights: " << sumW() << std::endl;
      std::cout << "SumOfWeights: " << sumOfWeights() << std::endl;
      std::cout << "CrossSection: " << crossSection() << std::endl;
      //std::cout << "CrossSectionPerEvent: " << crossSectionPerEvent() << "  " << picobarn << std::endl;
      //std::cout << "Cross section" <<crossSection()/picobarn << "\t Cross section per event" << crossSectionPerEvent()/picobarn << std::endl;
      //double xs = 1.0; // = crossSection()/picobarn;
      
      // This part does not work in pythia 6 <= 428.2
      //const double xs = crossSectionPerEvent()/picobarn;

      double XSection=1.0;
      //if( crossSection() == 987654321 )
      //{
      XSection=getXSfromLog();
	  //}
	  //else
	  //{
	  //XSection=crossSection();
	  //}
      cout << "Using cross section: " << XSection << endl;
      
      double genFiltEff=1.0;
      genFiltEff=getGetGenFiltEffFromLog();
      cout << "Using gen filter efficiency: : " << genFiltEff << endl;

      h_nFiles       -> Fill( 1, 1);
      h_xs           -> Fill( 1, XSection );
      h_nEvents      -> Fill( 1, evt );
      h_sumOfWeights -> Fill( 1, sumw );
      h_genFiltEff   -> Fill( 1, genFiltEff );
      
      file -> Write();
      file -> Close();
    }
    
  };
    
  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(JETS);
}

