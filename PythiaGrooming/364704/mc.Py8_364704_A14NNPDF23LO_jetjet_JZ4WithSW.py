# JO for Pythia 8 jet jet JZ4 slice

EFFECT=""
try:
    EFFECT=str(os.environ['NPC_EFFECT'])
except KeyError:
    print "WARNING: using default effect"
    EFFECT="Parton"
    print "ERROR: Wrong effect set in environment"

print 'EFFECT =', EFFECT

#############################                                                                          
#### Pythia8 defs                                                                                     
#if runArgs.runNumber==100000: #normal                                                                 
if EFFECT=='Parton':
    TYPE = 'NS'
   
#elif runArgs.runNumber==100001: #UE+Had                                                               
elif EFFECT=='UEHad':
    TYPE = 'AH'

#elif runArgs.runNumber==100002: #UE                                                                   
elif EFFECT=='UE':
    TYPE = 'AS'

#elif runArgs.runNumber==100003: #Hadr                                                                 
elif EFFECT=='Hadr':
    TYPE = 'NH'

elif EFFECT=='NOShower': ### special for Powheg+Pythia8                                                
    TYPE = 'OP'

else:
    print 'ERROR: Wrong EFFECT!'
    exit()

evgenConfig.description = "Dijet truth jet slice JZ4, with the A14 NNPDF23 LO tune"
evgenConfig.keywords = ["QCD", "jets", "SM"]
evgenConfig.contact = ["amoroso@cern.ch"]

if hasattr(testSeq, "TestHepMC"):
    testSeq.remove(TestHepMC())

include("MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("MC15JobOptions/Pythia8_ShowerWeights.py")
genSeq.Pythia8.Commands += ["HardQCD:all = on",
                            "PhaseSpace:Bias2Selection=on",
                            "PhaseSpace:pTHatMin = 150."]

ANALYSES = ['LeadingJets_WithSD']

from Rivet_i.Rivet_iConf import Rivet_i

rivet = Rivet_i()
    
rivet.AnalysisPath = os.getenv('PWD')
rivet.OutputLevel = INFO
rivet.Analyses = ANALYSES
genSeq += rivet





