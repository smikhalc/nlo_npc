import os

JS = 0

try:
    JS=int(os.environ['JS'])
except KeyError:
    print "WARNING: missing jSample number"
    exit()
    
print ('JSample = ', JS)
    
ktmin = { 1:2, 2:5, 3:15, 4:30, 5:75, 6:150, 7:250, 8:750, 9:1250 }
supp  = { 1:60, 2:160, 3:400, 4:800, 5:1800, 6:3200, 7:5300, 8:9000, 9:11000 }

#--------------------------------------------------------------
# EVGEN configuration
#--------------------------------------------------------------

evgenConfig.description = "POWHEG+Pythia8 dijet production with A14 NNPDF2.3 tune."
evgenConfig.keywords = ["SM", "QCD", "jets", "2jet"]
evgenConfig.contact = ["amoroso@cern.ch"]
evgenConfig.generators = ['Powheg']
evgenConfig.nEventsPerJob = 10

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg jj process
# --------------------------------------------------------------

import PowhegControl
transform_runArgs = runArgs if "runArgs" in dir() else None
transform_opts = opts if "opts" in dir() else None
PowhegConfig = PowhegControl.PowhegControl(process_name="jj", run_args=transform_runArgs, run_opts=transform_opts)
#include("PowhegControl/PowhegControl_jj_Common.py")

PowhegConfig.bornktmin = ktmin[JS]
PowhegConfig.bornsuppfact = supp[JS]
#PowhegConfig.nEvents = 10

PowhegConfig.mu_F         = [1.0]#, 2.0, 0.5, 1.0, 1.0, 0.5, 2.0, 0.5, 2.0] # List of factorisation scales which pairs with renormalisation scale below
PowhegConfig.mu_R         = [1.0]#, 1.0, 1.0, 2.0, 0.5, 0.5, 2.0, 2.0, 0.5] # List of renormalisation scales
PowhegConfig.PDF          = 260000#, 25200, 13165, 90900, 265000, 266000, 303400] # NNPDF30_nlo_as_0118, MMHT2014nlo68clas118, CT14nlo_as_0118, PDF4LHC15_nlo_30, NNPDF30_nlo_as_0117, NNPDF30_nlo_as_0119, NNPDF31_nlo_as_0118 - PDF variations with nominal scale variation


#include("PowhegControl/PowhegControl_jj_Common.py")
#PowhegConfig.bornktmin =2
#PowhegConfig.withnegweights =-1
#PowhegConfig.bornsuppfact =60
#PowhegConfig.nEvents = 10000
print PowhegConfig
   
# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------

PowhegConfig.generate()

exit()
