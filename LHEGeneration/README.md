The NP correction is usually calculated using Monte-Carlo (MC) event generators with phenomenological models for hadronisation and UE as the bin-wise ratio of the differential jet cross sections obtained with and without NP effects. 
Based on the above u need to generate spectra of particles with NP effects on and spectra of partons (ME after showering) with MC generators.
 
In case of NLO generators it will be better to generate LHE fales with information about ME kinematics and then shower them.
In this dir u can find scripts for Powheg LHE generation.
Directories 10000{$i} contain JO for sample i and integration grids file *GRID.tar.gz for dijet (inclusive jet) production. 
JS - 0 - test sample (I use them for test smthg new)
JS - 1-9 - production normal samples.
In case of changing process or adding smthg new u need to regenerate integration grid files (more info here - https://gitlab.cern.ch/atlas/athena/-/tree/21.6/Generators/PowhegControl/doc#re-using-integration-files-warning-section-contains-outdated-information-and-will-be-revised-soon-warning). For this u need to make first run of each sample and save .tar.gz file with name according to {mc_13TeV.Ph_dijets.GRID.tar.gz mc_{CME}.{GEN}_{Proc}.GRID.tar.gz}. Then put this file inside dir with JO and enjoy fast generation. Gen_tf macro automatically find this file if everythig ok and u find in log that preintegrated file found and added successfully. It will significantry reduce time for generation.
Also stick of the naming JO files in dir 10000{$i}. 

## More information about Powheg generation u can find here: https://gitlab.cern.ch/atlas/athena/-/tree/21.6/Generators/PowhegControl/doc
## I use standart ATLAS parameters with some changes, that Simone (simone.amoroso@cern.ch) recommended me.
## If u want to understand JO and parameters u can read next paper: https://arxiv.org/pdf/1303.3922.pdf

runProd.sh intended for sending tasks to PanDa.
rucioDown.sh intended for downloading finished files from PanDa.

## Initialize the enviromnent

```
source setup.sh
```

Current setup does
```
asetup AthGeneration,21.6.29, here
```

versions Powheg - 2_r3480

## To make check of correct working just make:

```
make --all
```

make --all does

```
Gen_tf.py --jobConfig=10000${JS} --ecmEnergy=13000 --randomSeed=${JS}1234 --maxEvents=10 --outputTXTFile=tst.PowhegOTF._1.events --env JS=${JS}
```

where --jobConfig - path to dir with JO;

      --ecmEnergy - ecmEnergy in MeV;
      --randomSeed - number for random generation sequences activation;

      --maxEvents - required number of events;

      --outputTXTFile -- name of output LHE file;
      
      -- --env -- special stuff for adding new variables in our case JS -- number of JS for generation. 

if test run works fine u can send massive production to PanDa using:
```
lsetup panda
```
lsetup rucio 
```
./runProd.sh
```
or
```
source runProd.sh
```
which contains next options:

--extFile -- add files which necessary for generation. This files will send to PanDa machine. Be careful Their size must be less than a certain fixed value. 

--split -- required number of jobs. Do not worry about seed, "--randomSeed=${i}%RNDM:0" autamatically set random for all splitted jobs.

--maxAttempt -- If your job fail, how many times PanDa will try to do them.

--outDS -- out naming format (with agreement of Gen_tf and future showering process https://gitlab.cern.ch/smikhalc/nlo_npc/-/tree/master/ShoweringPythia), then you will download this file from rucio with this name. 

## In https://bigpanda.cern.ch/user/ u can check status of current jobs.

## For downloading generated files u can use:
```
lsetup panda
lsetup rucio
```
```
./rucioDown.sh
```
or
```
source rucioDown.sh
```

A lot of work have been already done. Check /nfs/dust/atlas/user/star/Mikhalcov/Powheg_pt2 .
In this folder u can find 9 samples dir`s with corresponding LHE files (*._1.lhe.events) and showered TreeJets.root files with effects (inside folders *UEHad*) and without them (inside folders *Parton*). About showering u can find more info in https://gitlab.cern.ch/smikhalc/nlo_npc/-/tree/master/ShoweringPythia . 

