#/!bin/bash

for i in {1..9}
do
    pathena --trf "Gen_tf.py --jobConfig=10000${i} --ecmEnergy=13000 --randomSeed=${i}%RNDM:0 --maxEvents=100000 --outputTXTFile=%OUT.PowhegOTF._1.events --env JS=${i}" --extFile=10000{i},10000${i}/mc_13TeV.Ph_dijets_J${i}.GRID.tar.gz --split 400 --maxAttempt=3 --outDS=user.smikhalc.Ph_LHE_100000_JS${i}_pt2.vtest
done
