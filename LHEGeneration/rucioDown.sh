/!bin/bash                                                                                                                                                                                                 

###TODO u can use the same pattern name for all sending jobs, just add the corresponding variable before loop and then use them.                                                                            

let "JS = 1"
let "seed = 0"

for entry in `ls $search_dir*.lhe.*`; ### make loop over all files with *.lhe* pattern inside current dir.                                                                                                  
do
    if [ ! -e "user.smikhalc.PhPy8_LHE_100000_JS${JS}_Parton_${seed}.vtest_EXT0" ] ### if this file already exist do not download them.                                                                     
    then
        rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_Parton_${seed}_norm.vtest_EXT0;

        ### if you comment upper line and uncomment lower, u will check existance of file and in case non-existance send job for generation to PanDa.                                                       

        #pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=Parton" --ext\
File=RivetJETS.so,${entry} --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_Parton_${seed}_norm.vtest;                                                              
    fi
    if [ ! -e "user.smikhalc.PhPy8_LHE_100000_JS${JS}_NOShower_${seed}.vtest_EXT0" ]
    then
        rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_NOShower_${seed}_norm.vtest_EXT0;
        #pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=NOShower" --e\
xtFile=RivetJETS.so,$entry --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_NOShower_${seed}_norm.vtest                                                             
    fi
    if [ ! -e "user.smikhalc.PhPy8_LHE_100000_JS${JS}_UE_${seed}.vtest_EXT0" ]
    then
        rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_UE_${seed}_norm.vtest_EXT0;
        #pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=UE" --extFile\
=RivetJETS.so,$entry --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_UE_${seed}_norm.vtest                                                                         
    fi
    if [ ! -e "user.smikhalc.PhPy8_LHE_100000_JS${JS}_UEHad_${seed}.vtest_EXT0" ]
    then
        rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_UEHad_${seed}_norm.vtest_EXT0;
        #pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=UE+Had" --ext\
File=RivetJETS.so,$entry --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_UEHad_${seed}_norm.vtest                                                                  
    fi
    if [ ! -e "user.smikhalc.PhPy8_LHE_100000_JS${JS}_Hadr_${seed}.vtest_EXT0" ]
    then
        rucio download user.smikhalc:user.smikhalc.PhPy8_LHE_100000_JS${JS}_Hadr_${seed}_norm.vtest_EXT0;
        #pathena --trf "Gen_tf.py --jobConfig=100000 --ecmEnergy=13000 --randomSeed=${JS}${seed} --maxEvents=100000 --inputGeneratorFile=lhe --outputEVNTFile=%OUT.pool.root --env NPC_EFFECT=Hadr" --extFi\
le=RivetJETS.so,$entry --extOutFile=TreeJets.root --maxAttempt=3 --outDS=user.smikhalc.PhPy8_LHE_100000_JS${JS}_Hadr_${seed}_norm.vtest                                                                     
    fi
    let "seed = seed + 1" ### for loop overall files                                                                                                                                                        
done
